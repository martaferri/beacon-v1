# Beacon v1.x

This repository contains:

* the (Python 3.6+) [source code for Beacon version 1.x](src),
* instructions for a [local deployment](deploy) (using docker and docker-compose),
* the (Python 3.6+) [source code for a Beacon UI](ui).
