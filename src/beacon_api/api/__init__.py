"""Beacon API endpoints.

The endpoints reflect the specification provided by:
https://github.com/ga4gh-beacon/specification/blob/develop/beacon.md

Endpoints:
* ``/``and ``/info`` -  Information about the datasets in the Beacon;
* ``/query`` -  querying/filtering datasets in the Beacon;
"""
