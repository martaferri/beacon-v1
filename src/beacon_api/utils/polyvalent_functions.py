"""Functions used by different endpoints. 
 - To do basic operations
 - To parse the filters request
 - To manage access resolution
"""

import ast
import logging
import yaml
from pathlib import Path

from ..api.exceptions import BeaconBadRequest, BeaconServerError, BeaconForbidden, BeaconUnauthorised
from .. import __apiVersion__
# from ..conf.config import DB_SCHEMA

LOG = logging.getLogger(__name__)

# ----------------------------------------------------------------------------------------------------------------------
#                                         BASIC FUNCTIONS
# ----------------------------------------------------------------------------------------------------------------------

def create_prepstmt_variables(value):
    """Takes a value of how many prepared variables you want to pass a query
    and creates a string to put it in it"""
    dollars = []
    for element in range(value):
        element += 1
        variable = "$" + str(element)
        dollars.append(variable)

    return ", ".join(dollars)



def filter_exists(include_dataset, datasets):
    """Return those datasets responses that the `includeDatasetResponses` parameter decides.
    Look at the exist parameter in each returned dataset to established HIT or MISS.
    """
    if include_dataset == 'ALL':
        return datasets
    elif include_dataset == 'NONE':
        return []
    elif include_dataset == 'HIT':
        return [d for d in datasets if d['exists'] is True]
    elif include_dataset == 'MISS':
        return [d for d in datasets if d['exists'] is False]


def datasetHandover(dataset_name):
    """Return the datasetHandover with the correct name of the dataset."""
    datasetHandover = [ { "handoverType" : {
                                        "id" : "CUSTOM",
                                        "label" : "Dataset info"
                                    },
                                    "note" : "Dataset information and DAC contact details in EGA Website",
                                    "url" : f"https://ega-archive.org/datasets/{dataset_name}"
                                    } ]
    return datasetHandover


# ----------------------------------------------------------------------------------------------------------------------
#                                         YAML LOADER
# ----------------------------------------------------------------------------------------------------------------------

def find_yml_and_load(input_file):
    """Try to load the access levels yaml and return it as a dict."""
    file = Path(input_file)

    if not file.exists():
        LOG.error(f"The file '{file}' does not exist", file=sys.stderr)
        return

    if file.suffix in ('.yaml', '.yml'):
        with open(file, 'r') as stream:
            file_dict = yaml.safe_load(stream)
            return file_dict

    # Otherwise, fail
    LOG.error(f"Unsupported format for {file}", file=sys.stderr)



# ----------------------------------------------------------------------------------------------------------------------
#                                         ACCESS RELATED FUNCTIONS AND DICT
# ----------------------------------------------------------------------------------------------------------------------

def access_resolution(request, token, host, public_data, registered_data, controlled_data):
    """Determine the access level for a user.

    Depends on user bona_fide_status, and by default it should be PUBLIC.
    """
    permissions = []
    # all should have access to PUBLIC datasets
    # unless the request is for specific datasets
    if public_data:
        permissions.append("PUBLIC")
    access = set(public_data)  # empty if no datasets are given

    # for now we are expecting that the permissions are a list of datasets
    if registered_data and token["bona_fide_status"] is True:
        permissions.append("REGISTERED")
        access = access.union(set(registered_data))
    # if user requests public datasets do not throw an error
    # if both registered and controlled datasets are request this will be shown first
    elif registered_data and not public_data:
        if token["authenticated"] is False:
            # token is not provided (user not authed)
            raise BeaconUnauthorised(request, host, "missing_token", 'Unauthorized access to dataset(s), missing token.')
        # token is present, but is missing perms (user authed but no access)
        raise BeaconForbidden(request, host, 'Access to dataset(s) is forbidden.')
    
    
    if controlled_data and 'permissions' in token and token['permissions']:
        # The idea is to return only accessible datasets

        # Default event, when user doesn't specify dataset ids
        # Contains only dataset ids from token that are present at beacon
        controlled_access = set(controlled_data).intersection(set(token['permissions']))
        access = access.union(controlled_access)
        if controlled_access:
            permissions.append("CONTROLLED")
    # if user requests public datasets do not throw an error
    # By default permissions cannot be None, at worst empty set, thus this might never be reached
    elif controlled_data and not (public_data or registered_data):
        if token["authenticated"] is False:
            # token is not provided (user not authed)
            raise BeaconUnauthorised(request, host, "missing_token", 'Unauthorized access to dataset(s), missing token.')
        # token is present, but is missing perms (user authed but no access)
        raise BeaconForbidden(request, host, 'Access to dataset(s) is forbidden.')
    LOG.info(f"Accesible datasets are: {list(access)}.")
    return permissions, list(access)


async def fetch_datasets_access(db_pool, datasets):
    """Retrieve 3 list of the available datasets depending on the access type"""
    LOG.info('Retrieving info about the available datasets (id and access type).')
    public = []
    registered = []
    controlled = []
    async with db_pool.acquire(timeout=180) as connection:
        async with connection.transaction():
            datasets_query = None if datasets == "null" or not datasets else ast.literal_eval(datasets)
            try:
                query = f"""SELECT access_type, id, stable_id FROM public.beacon_dataset
                           WHERE coalesce(stable_id = any($1), true);
                           """
                LOG.debug(f"QUERY datasets access: {query}")
                statement = await connection.prepare(query)
                db_response = await statement.fetch(datasets_query)
                for record in list(db_response):
                    if record['access_type'] == 'PUBLIC':
                        public.append(record['id'])
                    if record['access_type'] == 'REGISTERED':
                        registered.append(record['id'])
                    if record['access_type'] == 'CONTROLLED':
                        controlled.append(record['id'])
                return public, registered, controlled
            except Exception as e:
                raise BeaconServerError(f'Query available datasets DB error: {e}')

