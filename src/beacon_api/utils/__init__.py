"""Utilities Module.

Contains utilities such as:
* ``polyvalent_functions.py`` -  Functions used in different endpoints;
* ``validate.py`` -  Validation of JSON schemas and request parameters;
* ``models.py`` -  Dictionaries of the different service models;
"""
